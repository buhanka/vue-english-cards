import Home from './components/Home.vue'
import Vocabulary from './components/Vocabulary.vue'

export const routes = [
  { path: '/', component: Home },
  { path: '/vocabulary', component: Vocabulary }
]
