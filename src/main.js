import Vue from 'vue'
import VeeValidate from 'vee-validate'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import App from './App'
import { routes } from './routes'

Vue.config.productionTip = false

Vue.use(VeeValidate)
Vue.use(VueResource)
Vue.use(VueRouter)

const router = new VueRouter({
  routes
})

Vue.http.options.root = 'https://vue-english-cards-a46b4.firebaseio.com/'

Vue.filter('capitalize', (value) => {
  let firstLetterUppercase = value[0].toUpperCase()
  value = value.split('')
  value.shift()
  value = value.join('')
  return firstLetterUppercase + value
})

export const eventBus = new Vue()

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
