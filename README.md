# english-cards

> Simple english vocabulary with nice animations, auto-translation and a search.

> Created with: Vue.js, Firebase, Vue Resource, Axios, Yandex Translate API and Animate.css

![alt text](https://raw.githubusercontent.com/cherenkor/vue-english-cards/master/preview.png)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
